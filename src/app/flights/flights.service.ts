import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {

  private flightUri='http://localhost:8080/flyaway/webapi/flights'

  constructor(private http:HttpClient, private router:Router) { }

  addFlight(flight){
    return this.http.post(this.flightUri,flight)
  }
  updateFlight(flight){
    return this.http.put(this.flightUri,flight)
  }

  deleteFlight(id){
    return this.http.delete(this.flightUri+"/"+id);
  }
  getAllFlight(){
    return this.http.get(this.flightUri)
  }


  
}





